module Music.MusicModel exposing (..)


type alias Score =
    { title : String
    , systems : List System
    }


type alias System =
    { key : Key
    , timeSig : TimeSig
    , upperStave : Stave
    , lowerStave : Stave
    }


type Key = Major Pitch | Minor Pitch


type TimeSig = TimeSig Int Duration


type alias Stave =
    { clef : Clef
    , notes : List Note
    }


type Clef = Treble | Bass


type alias Note =
    { pitch : Pitch
    , octave : Octave
    , duration : Duration
    , flat : Bool
    , sharp : Bool
    , natural : Bool
    }


type Pitch = A | B | C | D | E | F | G


type Octave = SubContra | Contra | Great | Small
    | First | Second | Third | Fourth | Fifth


type Duration = D1 | D2 | D4 | D8 | D16 | D32
