module Page.LessonModel exposing (..)

import Music.MusicModel


type alias LessonModel =
    { title : String
    , zoomStep : Int
    , scoreSize : ( Int, Int )
    , scoreViewBox : ( Int, Int )
    , currExercise : Music.MusicModel.Score
    }


type ZoomType = ZoomIn | ZoomOut


type LessonAction = Zoom ZoomType | ChooseExercise String
