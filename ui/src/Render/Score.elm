module Render.Score exposing (draw)

import Music.MusicModel exposing(Score, System, TimeSig(..))

import Render.ColorsHandbook as CH
import Render.Geom exposing (rect_)
import Render.SizesHandbook as SH
import Render.Stave as Stave

import String exposing (fromFloat, fromInt)

import Svg exposing (..)
import Svg.Attributes exposing (..)

import Utils.Utils as U


draw : Score -> List (Svg msg)
draw score =
    let
        numSystems = List.length score.systems
        vPositions = U.steps 0 SH.sizes.systemSpace numSystems
    in
    List.map2 drawSystem score.systems vPositions |> List.concat


drawSystem : System -> Float -> List (Svg msg)
drawSystem system vPos =
    let
        hPos = SH.sizes.scorePadding
        ( staveWidth, upperSvgs ) = Stave.draw system.upperStave hPos vPos
        yy = vPos + SH.sizes.staveSpace
        ( _, lowerSvgs ) = Stave.draw system.lowerStave hPos yy
        firstTactLine = drawTactLine hPos (vPos + SH.sizes.lineSpace * 2)
        tacts = drawTacts system staveWidth vPos
    in
    [[firstTactLine], tacts, upperSvgs, lowerSvgs ] |> List.concat


drawTacts : System -> Float -> Float -> List (Svg msg)
drawTacts system staveWidth vPosition =
    let
        (TimeSig numBeats beatDuration) = system.timeSig
        beatWidth = Stave.durationToWidth beatDuration * SH.sizes.quaterWidth
        tactWidth = toFloat numBeats * beatWidth
        numTacts = round (staveWidth / tactWidth)
        xx = SH.sizes.scorePadding + SH.sizes.firstTactPadding
        hPositions = U.steps xx tactWidth numTacts
    in
    List.map (\hPos -> drawTact hPos vPosition beatWidth numBeats) hPositions |> List.concat


drawTact : Float -> Float -> Float -> Int -> List (Svg msg)
drawTact hPos vPos beatWidth numBeats =
    let
        xx = hPos + toFloat numBeats * beatWidth - 1
        yy = vPos + SH.sizes.lineSpace * 2
        beats = drawBeat hPos yy beatWidth CH.colors.beatStrong numBeats
        tactLine = drawTactLine xx yy
    in
    beats ++ [ tactLine ]


drawBeat : Float -> Float -> Float -> String -> Int -> List (Svg msg)
drawBeat hPos vPos beatWidth clr beatsLeft =
    if beatsLeft == 0 then []
    else
        let
            hh = SH.sizes.lineSpace * 12
            beat = rect_ hPos vPos beatWidth hh clr
            nextClr =
                if clr == CH.colors.beatStrong then CH.colors.beatWeak
                else CH.colors.beatStrong
        in
        beat :: drawBeat (hPos + beatWidth) vPos beatWidth nextClr (beatsLeft - 1)


drawTactLine : Float -> Float -> Svg msg
drawTactLine hPos vPos =
    let
        hh = SH.sizes.lineSpace * 12
    in
    rect_ hPos vPos 2.0 hh CH.colors.tactLine

