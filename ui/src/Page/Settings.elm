module Page.Settings exposing (view)

import Html exposing (..)


view : Html msg
view =
    div []
        [ h2 [] [ text "Settings" ]
        , p [] [ text "this is Settings Page" ]
        ]
