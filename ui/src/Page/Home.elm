module Page.Home exposing (view)

import Html exposing (..)


view : Html msg
view =
    div []
        [ h2 [] [ text "Home" ]
        , p [] [ text "this is Home Page" ]
        ]
