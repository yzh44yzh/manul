module Page.PageSwitcher exposing (view)

import AppModel exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)

import Page.About
import Page.Home
import Page.Lesson
import Page.Settings


view : AppModel -> Html AppAction
view model =
    div []
        [ div [ class "ui top attached tabular menu" ]
            [ tab Home model.currentPage "Home"
            , tab Lesson model.currentPage "Lesson"
            , tab Settings model.currentPage "Settings"
            , tab About model.currentPage "About"
            ]
        , div [ class "ui bottom attached active tab segment" ]
            [ case model.currentPage of
                Home -> Page.Home.view
                Lesson -> Html.map LessonAction (Page.Lesson.view model.lessonModel)
                Settings -> Page.Settings.view
                About -> Page.About.view
            ]
        ]


tab : Page -> Page -> String -> Html AppAction
tab page currPage title =
    div
        [ class <|
            if page == currPage then "active item"
            else "item"
        , onClick <| SwitchPage page
        ]
        [ text title ]
