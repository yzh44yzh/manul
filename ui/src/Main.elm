module Main exposing (main, update, view)

import AppModel exposing (AppAction(..), AppModel)
import Browser
import Html exposing (Html)
import Page.Lesson
import Page.PageSwitcher


main : Program () AppModel AppAction
main =
    Browser.sandbox
        { init = AppModel AppModel.Lesson Page.Lesson.init
        , update = update
        , view = view
        }


update : AppAction -> AppModel -> AppModel
update action model =
    case action of
        SwitchPage page ->
            { model | currentPage = page }
        LessonAction lessonAction ->
            { model | lessonModel = Page.Lesson.update lessonAction model.lessonModel }


view : AppModel -> Html AppAction
view model =
    Page.PageSwitcher.view model
