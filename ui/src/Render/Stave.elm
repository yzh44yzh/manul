module Render.Stave exposing (draw, durationToWidth)

import Music.MusicModel as MM

import Render.ColorsHandbook as CH
import Render.Geom exposing (rect_)
import Render.Note as Note
import Render.SizesHandbook as SH

import Svg exposing (..)
import Svg.Attributes exposing (..)

import String exposing (fromFloat, fromInt)
import Utils.Utils as U


draw : MM.Stave -> Float -> Float -> ( Float, List (Svg msg) )
draw stave hPos vPos =
    let
        xx = hPos + SH.sizes.firstTactPadding
        qw = SH.sizes.quaterWidth
        ( notesWidth, notesWithPositions ) = addPositionsForNotes xx stave.notes
        numQuaters = notesWidth / qw
        staveWidth = notesWidth + SH.sizes.firstTactPadding
    in
    ( staveWidth
    , (U.range2 2 6 |> List.map (noteLine hPos vPos staveWidth))
        ++ List.map (Note.draw stave.clef vPos) notesWithPositions
    )


addPositionsForNotes : Float -> List MM.Note -> ( Float, List ( MM.Note, Float ) )
addPositionsForNotes hPos notes =
    let
        qw = SH.sizes.quaterWidth
        f note ( lastPos, notesWithPos ) =
            let
                notesWithPos2 =
                    ( note, lastPos ) :: notesWithPos

                nextPos =
                    lastPos + qw * durationToWidth note.duration
            in
            ( nextPos, notesWithPos2 )

        ( lastPos_, notesWithPos_ ) =
            List.foldl f ( hPos, [] ) notes
    in
    ( lastPos_ - hPos, List.reverse notesWithPos_ )


durationToWidth : MM.Duration -> Float
durationToWidth duration =
    case duration of
        MM.D1 -> 4
        MM.D2 -> 2
        MM.D4 -> 1
        MM.D8 -> 0.5
        MM.D16 -> 0.25
        MM.D32 -> 0.125


noteLine : Float -> Float -> Float -> Float -> Svg msg
noteLine hPos vPos staveWidth pos =
    rect_ hPos (vPos + pos * SH.sizes.lineSpace) staveWidth 1.5 CH.colors.noteLine
