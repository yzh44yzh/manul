module Page.Lesson exposing (init, update, view)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Svg as S
import Svg.Attributes as SA

import Music.Exercise
import Music.OPeterson

import Page.LessonModel exposing (..)

import Render.Score
import Render.SizesHandbook as SH

import String exposing (fromFloat, fromInt)


init : LessonModel
init =
    LessonModel "Lesson 1" 50 ( 1100, 420 ) ( 1100, 420 ) Music.Exercise.ex1


update : LessonAction -> LessonModel -> LessonModel
update action ({ zoomStep, scoreViewBox, currExercise } as model) =
    let
        (vbWidth, vbHeight) = scoreViewBox
    in
    case action of
        Zoom ZoomIn ->
            { model | scoreViewBox = ( vbWidth - zoomStep, vbHeight - zoomStep ) }
        Zoom ZoomOut ->
            { model | scoreViewBox = ( vbWidth + zoomStep, vbHeight + zoomStep ) }
        ChooseExercise "ex1" ->
            { model | currExercise = Music.Exercise.ex1 }
        ChooseExercise "ex2" ->
            { model | currExercise = Music.Exercise.ex2 }
        ChooseExercise "ex3" ->
            { model | currExercise = Music.OPeterson.ex1 }
        ChooseExercise _ ->
            { model | currExercise = Music.Exercise.ex1 }


view : LessonModel -> Html LessonAction
view lessonModel =
    div [ class "manul-centered" ]
        [ h2 [] [ text lessonModel.currExercise.title ]
        , p [] [ text <| showZoom lessonModel ]
        , drawScoreViewBox lessonModel
        , drawToolBar
        ]


showZoom : LessonModel -> String
showZoom { scoreSize, scoreViewBox } =
    let
        (sw, sh) = scoreSize
        (vw, vh) = scoreViewBox
    in
    "Model: (" ++ fromInt sw ++ "/" ++ fromInt sh ++ ") "
        ++ "(" ++ fromInt vw ++ "/" ++ fromInt vh ++ ")"


drawScoreViewBox : LessonModel -> S.Svg msg
drawScoreViewBox { currExercise, scoreSize, scoreViewBox } =
    let
        (scoreWidth, scoreHeight) = scoreSize
        sw = fromInt scoreWidth
        sh = fromInt scoreHeight
        (vbWidth, vbHeight) = scoreViewBox
        vbw = fromInt vbWidth
        vbh = fromInt vbHeight
    in
    S.svg
        [ SA.class "manul-score"
        , SA.width <| sw
        , SA.height <| sh
        , SA.viewBox <| "0 0 " ++ vbw ++ " " ++ vbh
        , SA.preserveAspectRatio "xMinYMin slice"
        ]
        (Render.Score.draw currExercise)


drawToolBar : Html LessonAction
drawToolBar =
    div [ class "manul-toolbar" ]
        [ span [ class "manul-toolbar-group" ]
            [ button
                [ class "ui orange basic button"
                , onClick <| Zoom ZoomIn
                ]
                [ text "+" ]
            , button
                [ class "ui orange basic button"
                , onClick <| Zoom ZoomOut
                ]
                [ text "-" ]
            ]
        , span [ class "manul-toolbar-group" ]
            [ button [ class "ui orange button" ]
                [ text "Play" ]
            , button
                [ class "ui orange button"
                , onClick <| ChooseExercise "ex1"
                ]
                [ text "Exercise 1" ]
            , button
                [ class "ui orange button"
                , onClick <| ChooseExercise "ex2"
                ]
                [ text "Exercise 2" ]
            , button
                [ class "ui orange button"
                , onClick <| ChooseExercise "ex3"
                ]
                [ text "Peterson" ]
            ]
        ]
