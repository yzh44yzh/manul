module Music.OPeterson exposing (ex1)

import Music.MusicModel exposing (..)


note : Pitch -> Octave -> Duration -> Note
note p o d =
    Note p o d False False False


ex1 : Score
ex1 =
    Score "Oscar Peterson. Jazz Etudes and Pieces for piano. Exercise N1."
        [ System
            (Major G)
            -- F#
            (TimeSig 4 D4)
            (Stave Treble
                [ note B First D8 -- 3 TODO: аппликатура
                , note D Second D8 -- 5
                , note B First D8 -- 3
                , note D Second D8 -- 5
                , note B First D8 -- 3
                , note D Second D8 -- 5
                , note B First D8 -- 3
                , note D Second D8 -- 5
                , note A First D8 -- 2
                , note D Second D8 -- 5
                , note A First D8 -- 2
                , note D Second D8 -- 5
                , note A First D8 -- 2
                , note D Second D8 -- 5
                , note A First D8 -- 2
                , note D Second D8 -- 5
                , note G First D8 -- 1
                , note B First D8 -- 3
                , note G First D8 -- 1
                , note B First D8 -- 3
                , note G First D8 -- 1
                , note B First D8 -- 3
                , note G First D8 -- 1
                , note B First D8 -- 3
                , Note F First D8 True False False -- 1
                , note B First D8 -- 4
                , Note F First D8 True False False -- 1
                , note B First D8 -- 4
                , Note F First D8 True False False -- 1
                , note B First D8 -- 4
                , Note F First D8 True False False -- 1
                , note B First D8 -- 4
                ]
            )
            (Stave Bass
                [ note G Small D1
                , Note F Small D1 True False False
                , note E Small D1
                , note D Small D1
                ]
            )
        , System
            (Major G)
            (TimeSig 4 D4)
            (Stave Treble
                [ note E First D8 -- 1
                , note G First D8 -- 3
                , note E First D8 -- 1
                , note G First D8 -- 3
                , note D First D8 -- 1
                , note G First D8 -- 3
                , note D First D8 -- 1
                , note G First D8 -- 3
                , note E First D8 -- 1
                , note G First D8 -- 3
                , note E First D8 -- 1
                , note G First D8 -- 3
                , note D First D8 -- 1
                , note G First D8 -- 3
                , note D First D8 -- 1
                , note G First D8 -- 3
                , note E First D8 -- 1
                , note G First D8 -- 3
                , note E First D8 -- 1
                , note G First D8 -- 3
                , note D First D8 -- 1
                , note G First D8 -- 3
                , note E First D8 -- 1
                , note G First D8 -- 3
                , note D First D8 -- 1
                , note B First D8 -- 5
                , note C First D8 -- 1
                , note A First D8 -- 4
                , note B Small D2 -- TODO: accord with G First
                ]
            )
            (Stave Bass
                [ note C Small D2
                , note B Great D2
                , note C Small D2
                , note B Great D2
                , note C Small D2
                , note B Great D4
                , note C Small D4
                , note D Small D4
                , note D Great D4
                , note G Great D2
                ]
            )
        ]
