module Render.SizesHandbook exposing (..)


type alias SizesHandbook =
    { scorePadding : Float
    , firstTactPadding : Float
    , quaterWidth : Float
    , lineSpace : Float
    , staveSpace : Float
    , systemSpace : Float
    , note : NoteSizes
    , alteration : AlterationSizes
    }


type alias NoteSizes =
    { imgXShift : Float
    , imgYShift : Float
    , imgWidth : Float
    }


type alias AlterationSizes =
    { shift : Float
    , sharpYShift : Float
    , sharpWidth : Float
    , flatYShift : Float
    , flatWidth : Float
    , naturalYShift : Float
    , naturalWidth : Float
    }


sizes : SizesHandbook
sizes =
    { scorePadding = 60.0
    , firstTactPadding = 80.0
    , quaterWidth = 50.0
    , lineSpace = 16.0
    , staveSpace = 16.0 * 8
    , systemSpace = 300.0
    , note =
        { imgXShift = -9.0
        , imgYShift = -4.0
        , imgWidth = 31
        }
    , alteration =
        { shift = 10
        , sharpYShift = 33
        , sharpWidth = 15
        , flatYShift = 33
        , flatWidth = 8
        , naturalYShift = 32
        , naturalWidth = 7
        }
    }
