module Example exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Test exposing (..)

import Utils.Utils as U

-- http://package.elm-lang.org/packages/elm-community/elm-test/latest

suite : Test
suite =
    describe "Utils module"
        [ describe "Utils.range2"
            [ test "range 1-5" <|
                \_ -> Expect.equal [1,2,3,4,5] (U.range2 1 5)
            , test "range 3-4" <|
                \_ -> Expect.equal [3,4,5] (U.range2 3 4)
            ]
        ]
