module Render.ColorsHandbook exposing (..)


type alias Color = String


type alias ColorsHandbook =
    { noteLine : Color
    , tactLine : Color
    , beatStrong : Color
    , beatWeak : Color
    }


colors : ColorsHandbook
colors =
    { noteLine = "#666"
    , tactLine = "#000"
    , beatStrong = "#fff5cc"
    , beatWeak = "#feffee"
    }
