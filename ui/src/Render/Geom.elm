module Render.Geom exposing (rect_)

import String exposing (fromFloat, fromInt)

import Svg exposing (..)
import Svg.Attributes exposing (..)


rect_ : Float -> Float -> Float -> Float -> String -> Svg msg
rect_ xx yy ww hh color =
    rect
        [ x <| fromFloat xx
        , y <| fromFloat yy
        , width <| fromFloat ww
        , height <| fromFloat hh
        , fill color
        ]
        []
