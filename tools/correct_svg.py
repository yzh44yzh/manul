path = "m 5.0389217,33.96487 c -2.2008625,1.399088 -3.1623615,3.520029 -2.1676508,4.871989 1.0647162,1.438363 3.9685518,1.480473 6.4824938,0.08541 C 11.868605,37.533092 13.043985,35.231605 11.97927,33.793241 10.921506,32.3539 8.0176693,32.31179 5.5037274,33.706861 c -0.157852,0.08241 -0.3235556,0.159902 -0.4648057,0.258018 z"

shift_x = 0
shift_y = 0

def correct(word):
    parts = word.split(",")
    if len(parts) == 2:
        x = float(parts[0])
        y = float(parts[1])
        return "{:.2f},{:.2f}".format(x - shift_x, y - shift_y)
    else:
        return word

words = map(correct, path.split(" "))
res = " ".join(words)
print(res)
