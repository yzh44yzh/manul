module Music.Exercise exposing (ex1, ex2)

import Music.MusicModel exposing (..)


note : Pitch -> Octave -> Duration -> Note
note p o d =
    Note p o d False False False


ex1 : Score
ex1 =
    Score
        "Exercise 1, Pitches & Octaves"
        [ System (Major A)
            (TimeSig 4 D4)
            (Stave Treble
                [ note A Small D4
                , note B Small D4
                , note C First D4
                , note D First D4
                , Note E First D4 True False False
                , Note F First D4 False True False
                , Note G First D4 False False True
                , Note A First D4 True False False
                , note B First D4
                , note C Second D4
                , note D Second D4
                , note E Second D4
                , note F Second D4
                , note G Second D4
                , note A Second D4
                , note B Second D4
                ]
            )
            (Stave Bass
                [ note A Great D4
                , note B Great D4
                , note C Small D4
                , note D Small D4
                , note E Small D4
                , note F Small D4
                , note G Small D4
                , note A Small D4
                , note B Small D4
                , note C First D4
                , note D First D4
                , note E First D4
                , note F First D4
                , note G First D4
                , note A First D4
                , note B First D4
                ]
            )
        , System (Major A)
            (TimeSig 4 D4)
            (Stave Treble
                [ note A Small D4
                , note B Small D4
                , note C First D4
                , note D First D4
                , note E First D4
                , note F First D4
                , note G First D4
                , note A First D4
                , note B First D4
                , note C Second D4
                , note D Second D4
                , note E Second D4
                , note F Second D4
                , note G Second D4
                , note A Second D4
                , note B Second D4
                ]
            )
            (Stave Bass
                [ note A Contra D4
                , note B Contra D4
                , note C Great D4
                , note D Great D4
                , note E Great D4
                , note F Great D4
                , note G Great D4
                , note A Great D4
                , note B Great D4
                , note C Small D4
                , note D Small D4
                , note E Small D4
                , note F Small D4
                , note G Small D4
                , note A Small D4
                , note B Small D4
                ]
            )
        ]


ex2 : Score
ex2 =
    Score "Exercise 2, Durations"
        [ System
            (Major A)
            (TimeSig 4 D8)
            (Stave Treble
                [ note C First D4
                , note C First D4
                , note G First D2
                , note F First D1
                , note F First D4
                , note G First D16
                , note A First D16
                , note B First D4
                , note C Second D8
                , note B First D4
                , note D Second D8
                , note E Second D8
                , note F Second D4
                , note G Second D2
                ]
            )
            (Stave Bass
                [ note C Small D4
                , note C Small D4
                , note G Small D4
                , note G Small D4
                , note C Small D4
                , note D Small D4
                , note E Small D4
                , note F Small D4
                , note G Small D4
                , note A Small D4
                , note B Small D4
                , note C First D4
                , note D First D4
                , note E First D4
                , note F First D4
                , note G First D4
                ]
            )
        , System
            (Major A)
            (TimeSig 4 D8)
            (Stave Treble
                [ note C First D4
                , note C First D4
                , note G First D2
                , note F First D1
                , note F First D4
                , note G First D16
                , note A First D16
                , note B First D4
                , note C Second D8
                , note B First D4
                , note D Second D8
                , note E Second D8
                , note F Second D4
                , note G Second D2
                ]
            )
            (Stave Bass
                [ note C Small D4
                , note C Small D4
                , note G Small D4
                , note G Small D4
                , note C Small D4
                , note D Small D4
                , note E Small D4
                , note F Small D4
                , note G Small D4
                , note A Small D4
                , note B Small D4
                , note C First D4
                , note D First D4
                , note E First D4
                , note F First D4
                , note G First D4
                ]
            )
        ]
