module AppModel exposing (..)

import Page.LessonModel exposing (LessonModel, LessonAction)


type Page = Home | Lesson | Settings | About


type alias AppModel =
    { currentPage : Page
    , lessonModel : LessonModel
    }


type AppAction = SwitchPage Page | LessonAction LessonAction
