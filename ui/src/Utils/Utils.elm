module Utils.Utils exposing (range2, range3, steps)


range2 : Float -> Float -> List Float
range2 from to =
    range3 from 1 to


range3 : Float -> Float -> Float -> List Float
range3 from step to =
    if from > to then
        []

    else
        from :: range3 (from + step) step to


steps : Float -> Float -> Int -> List Float
steps from step numTimes =
    range3 from step <| (*) step <| toFloat <| numTimes
