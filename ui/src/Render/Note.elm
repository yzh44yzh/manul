module Render.Note exposing (draw)

import Music.MusicModel exposing(..)

import Render.SizesHandbook as SH

import String exposing (fromFloat, fromInt)

import Svg exposing (..)
import Svg.Attributes exposing (..)


draw : Clef -> Float -> ( Note, Float ) -> Svg msg
draw clef vPos ( note, pos ) =
    svg
        [ x <| fromFloat <| pos + SH.sizes.note.imgXShift - SH.sizes.alteration.shift
        , y <| fromFloat <| vPos + getVPosition clef note
        ]
        (case (note.sharp, note.flat, note.natural) of
            (True, False, False) -> [ drawNote note, drawSharp ]
            (False, True, False) -> [ drawNote note, drawFlat ]
            (False, False, True) -> [ drawNote note, drawNatural ]
            _ -> [ drawNote note ]
        )


drawNote : Note -> Svg msg
drawNote note =
    image
        [ x <| fromFloat SH.sizes.alteration.shift
        , width <| fromFloat SH.sizes.note.imgWidth
        , xlinkHref <| noteImg note.duration
        ]
        []


drawSharp : Svg msg
drawSharp =
    image
        [ x "0"
        , y <| fromFloat SH.sizes.alteration.sharpYShift
        , width <| fromFloat SH.sizes.alteration.sharpWidth
        , xlinkHref "./resources/sharp.svg"
        ]
        []


drawFlat : Svg msg
drawFlat =
    image
        [ x "0"
        , y <| fromFloat SH.sizes.alteration.flatYShift
        , width <| fromFloat SH.sizes.alteration.flatWidth
        , xlinkHref "./resources/flat.svg"
        ]
        []


drawNatural : Svg msg
drawNatural =
    image
        [ x "0"
        , y <| fromFloat SH.sizes.alteration.naturalYShift
        , width <| fromFloat SH.sizes.alteration.naturalWidth
        , xlinkHref "./resources/natural.svg"
        ]
        []


getVPosition : Clef -> Note -> Float
getVPosition clef note =
    let
        pitchStep = SH.sizes.lineSpace / 2
        pitchPos = pitchStep * pitchShift note.pitch
        octaveStep = pitchStep * 7
        octavePos = octaveStep * octaveShift note.octave
        clefShift =
            case clef of
                Treble -> 0
                Bass -> -pitchStep * 12
    in
    octavePos + pitchPos + SH.sizes.note.imgYShift + clefShift


pitchShift : Pitch -> Float
pitchShift pitch =
    case pitch of
        C -> 9
        D -> 8
        E -> 7
        F -> 6
        G -> 5
        A -> 4
        B -> 3


octaveShift : Octave -> Float
octaveShift octave =
    case octave of
        SubContra -> 4
        Contra -> 3
        Great -> 2
        Small -> 1
        First -> 0
        Second -> -1
        Third -> -2
        Fourth -> -3
        Fifth -> -4


noteImg : Duration -> String
noteImg duration =
    case duration of
        D1 -> "./resources/n1.svg"
        D2 -> "./resources/n2.svg"
        D4 -> "./resources/n4.svg"
        D8 -> "./resources/n8.svg"
        D16 -> "./resources/n16.svg"
        D32 -> "./resources/n32.svg"
