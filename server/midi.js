// https://webaudio.github.io/web-midi-api/
console.log("request MIDI access");

if (navigator.requestMIDIAccess) {
    navigator.requestMIDIAccess({
        sysex: false
    }).then(onMIDISuccess, onMIDIFailure);
} else {
    alert("No MIDI support in your browser.");
}

function onMIDISuccess(midiAccess /* MIDIAccess */) {
    console.log("onMIDISuccess", midiAccess);
    midiAccess.onchangestate = onMIDIChangeState;
    for (var entry of midiAccess.inputs) {
        console.log(entry);
        var input /* MIDIInput */ = entry[1];
        input.onmidimessage = onMIDIMessage;
        input.onstatechange = onInputChangeState;

        var info = input.name + " (" + input.manufacturer + "): " + input.state;
        document.getElementById('midi_input').innerHTML = info;
    }
}

function onMIDIChangeState(e) {
    console.log("onMIDIChangeState", e);
}

function onInputChangeState(e) {
    console.log("onInputChangeState", e);
}

function onMIDIFailure(error) {
    console.log("No access to MIDI devices or your browser doesn't support WebMIDI API");
}

function onMIDIMessage(message) {
    var d = message.data;
    if (d[0] == 144 || d[0] == 128) {
        console.log('MIDI data', message.data); // MIDI data [144, 63, 73]
        var info = "Down: ";
        if (d[0] == 128) info = "Up: ";
        info += "note " + d[1] + " velocity " + d[2];
        document.getElementById('midi_event').innerHTML = info;
    }
}
