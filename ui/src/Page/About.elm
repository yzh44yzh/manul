module Page.About exposing (view)

import Html exposing (..)
import Html.Attributes exposing (..)


view : Html msg
view =
    div [ class "manul-centered" ]
        [ img
            [ width 600
            , src "./resources/manul.jpg"
            ]
            []
        , h3 [] [ text "Manul Piano Tutor" ]
        , p [] [ text "version 0.1" ]
        ]
